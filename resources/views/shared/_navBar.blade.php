<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
    <script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

    @yield('styles')

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header class="container" style="    height: 62px;">
            <div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="ml-5 navbar-brand" href="#">
                        <img src="{{ asset('images/Captureaaa.png') }}" width="50" height="50" alt="">
                        <label class="p-0" for=""><small style="color: #5f4879; font-weight: 400; font-size: 20px; p-0">NovHôtel</small></label>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse "  id="navbarSupportedContent">
                        <ul class="navbar-nav  mr-5" style="margin-left: auto;">
                            <li class="nav-item active  ">
                                <a class="nav-link ml-4" href="#" style="font-weight:bolder; font-size:14px;" >Besoin d'aide ? <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link ml-4" href="#"   style="font-weight: bold; font-size: 14px;">Nos chambres</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link ml-4" href="#"   style="font-weight: bold;  font-size: 14px;">FR | EN</a>
                            </li>
                            <li class="nav-item active">
                                <a class="dropdown-item" href="{{ route('login') }}" style="background: white;
                                font-weight: bold;
                                font-size: 14px;
                                outline: none;

                                box-shadow: none;
                                color: gray;
                                margin-top: 4px;"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                 {{ __('Se Déconnecter') }}
                             </a>

                             <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                 @csrf
                             </form>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link ml-3" href="#"  style="font-weight: bold; font-size: 14px;"><i class="fa fa-bars" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
         </header>

    <main class="">
        @yield('content')
    </main>
    </div>
</body>

@yield('extrat-js')
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>

</html>
