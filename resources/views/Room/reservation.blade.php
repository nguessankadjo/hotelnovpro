@extends('shared._navBar')

@section('styles')
    <link href="{{ asset('css/styledetailReservation.css') }}" rel="stylesheet">
@stop

@section('content')
<section class="banner">
    <div class="container">
    <div class="row">
        <div class="semd bg-white"  style="    width: 70.2em;
        margin: auto;
        margin-top: -231px;
        border-radius: 20px;
        border: solid 1px;">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8">
                            <div style="border-radius: 31px;
                            padding: 6px 13px;
                            background: white;
                            position: absolute;
                            opacity: 0.5;
                            margin-left: 17px;
    margin-top: 23px;"><a href="{{ route('showAll') }}"><i style="color: black" class="fa fa-chevron-left" aria-hidden="true"></i></a></div>
                            <div style="margin-left: 15px;margin-top: 24px;">
                                <img src="{{ asset('images/imga6.png') }}" alt="image reservation 1" style="width: 36.8em;
                                height: 289px;
                                border-radius: 14px;
                                margin-left: -14px;
                                margin-top: -12px;">
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-left: 40px;
                        margin-right: -41px;">
                            <div class="card" style="padding: 0px;
                            border: none;
                            margin-bottom: -15px;
                            margin-left: 3.9em;
                            margin-top: 24px;">
                                <div class="card-body" style="padding: 0px;    margin-top: -10px;">
                                    <img src="{{ asset('images/c7.png') }}" alt="reservation 2" style="height: 5.6em;
                            width: 11.8em;
                            border-radius: 10px;">
                                </div>
                            </div>
                            <div class="card" style="padding: 0px;
                            border: none;
                            margin-bottom: -15px;
                            margin-left: 3.9em;
                            margin-top: 24px;">
                                <div class="card-body" style="padding: 0px;">
                                    <img src="{{ asset('images/c7.png') }}" alt="reservation 3" style="height: 5.6em;
                            width: 11.8em;
                            border-radius: 10px;">
                                </div>
                            </div>
                            <div class="card" style="padding: 0px;
                            border: none;
                            margin-bottom: -11px;
                            margin-left: 3.9em;
                            margin-top: 24px;">
                                <div class="card-body" style="padding: 0px;">
                                    <img src="{{ asset('images/c7.png') }}" alt=" reservation 4" style="height: 5.6em;
                            width: 11.8em;
                            border-radius: 10px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div style="margin-left: 4px;
                        margin-top: 6px;">
                            <span style="font-family: sans-serif;
                            font-size: 26px;
                            font-weight: bold;">{{  $getRoomid->titre }}</span>
                            <div class="d-flex" style="margin-top: 16px;">
                                <div><img src="{{ asset('images/14.png') }}" alt="" width="60px" height="60px" style="    margin-right: 18px;
                                    border-radius: 6px;
                                    border: solid 2px;
                                    border-color: #9ab8d1;"></div>
                                <div><img src="{{ asset('images/15.png') }}" alt="" width="60px" height="60px" style="    margin-right: 18px;
                                    border-radius: 6px;
                                    border: solid 2px;
                                    border-color: #9ab8d1;"></div>
                                <div><img src="{{ asset('images/16.png') }}" alt="" width="60px" height="60px" style="    margin-right: 18px;
                                    border-radius: 6px;
                                    border: solid 2px;
                                    border-color: #9ab8d1;"></div>
                            </div>
                        </div>
                        <p style="margin-left: 26.2em;
                        margin-top: -2.6em;
                        font-weight: bold;">
                            <i style="color: gold;" class="fa fa-star" aria-hidden="true"></i> 4.9
                        </p>

                    </div>
                    <div class="row">
                        <div style="    margin-top: 57px;">
                            <div>
                            <p style="color:#9ab8d1 ;font-size: 15px; font-weight: bold;">Description</p>
                            </div>
                            <div >
                                <span>{{  $getRoomid->description }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="    margin-left: 34px;
                    margin-right: -51px;
                    margin-top: -7px;">
                        <form action="{{ route('saveReservation.create', $id) }}" method="POST" >
                            @csrf()
                            @method('POST')
                        <div class="card" style="width: 18rem;
                            margin-top: 20px;
                            border: solid #a2e8fe;
                            border-style: solid;
                            margin-bottom: 24px;
                            border-radius: 11px;
                            margin-left: 25px;">
                            <div class="card-body">
                              <p class="card-title"><span style="font-size: 20px;font-weight: bolder;">{{  $getRoomid->prix }} F<span> <small style="font-size: 22px;">/Semaine</small></p>
                              <hr style="margin-top: 7px;">
                              <p class="card-text" style="color: #07528d; font-size:15px;font-weight: bold;">Detail de reservation</p>
                              <div>
                                <small style="opacity: 0.5;">Date d'arrivée</small>
                              </div>
                              <div>
                                <i class="fa fa-calendar-check" aria-hidden="true" style="color: gray;
                                font-size: 11px;
                                margin-left: 12px;
                                opacity: 0.5;"></i>
                                <input type='text' id='dateArrive' name ="date_arrive" value="{{ \Carbon\Carbon::now()->isoFormat('D  MMMM YYYY') }}" style="border: none;
                                color: gray;
                                opacity: 0.5;
                                font-size: 13px; outline: none " autocomplete="off" onFocus="javascript:this.value=''"  />
                              </div>
                              <div style="margin-top: 15px;">
                                <small style="opacity: 0.5;">Date de depart</small>
                              </div>

                                <div>
                                    <i class="fa fa-calendar-check" aria-hidden="true" style="color: gray;
                                    font-size: 11px;
                                    margin-left: 12px;
                                    opacity: 0.5;"></i>
                                    <input type='text' id='dateDepart' name="date_depart" value="{{ \Carbon\Carbon::now()->isoFormat('D  MMMM YYYY') }}" style="border: none;
                                    color: gray;
                                    opacity: 0.5;
                                    font-size: 13px; outline: none "  autocomplete="off" onFocus="javascript:this.value=''" />
                                </div>
                                <div  style="margin-top: 15px;">
                                    <small style="opacity: 0.5;">Invité</small>
                                </div>

                              <div>
                                <i class="fa fa-calendar-check" aria-hidden="true" style="color: gray;
                                font-size: 11px;
                                margin-left: 12px;
                                opacity: 0.5;"></i>
                                <input type='text' name="nombre_invite" value="2 Aldults" style="border: none;
                                color: gray;
                                opacity: 0.5;
                                font-size: 13px; outline: none "  onFocus="javascript:this.value=''" />
                                <button type="button" class="btn btn-secondary" data-bs-toggle="tooltip" data-bs-placement="top" title="Saisir le nombre d'invité" style="background: white;
                                color: #07528d;
                                font-weight: bold;
                                border: none;
                                outline: none;
                                box-shadow: none;
                                margin-right: -3px;
                                margin-left: -10px;">
                                    Modifier
                                  </button>
                              </div>

                              <p class="card-text" style="color: #07528d; font-size:15px;font-weight: bold;    margin-top: 6px;
                              margin-bottom: 4px">Récapitulatif</p>
                              <div style="padding: 10px;background: #f2f7f9;">
                                  <div style="    margin-bottom: 8px;">
                                    <span style="margin-right: 83px;font-size: 15px;">1 Nuit(s)</span><span style="font-weight: bold;">{{  $getRoomid->prix }} F</span>
                                  </div>
                                  <div>
                                    <span style="margin-right: 121px;font-size: 15px;">Reduction</span><span style="font-weight: bold;">0.0F</span>
                                  </div>
                              </div>
                              <div style="margin-top: 15px;margin-left: 7px;margin-bottom: 15px;">
                                <small style="margin-right: 45px">Total paiement</small> <span style="font-weight: bold;">{{  $getRoomid->prix }} F</span>
                              </div>
                              <p class="card-text" style="color: #07528d; font-size:15px;font-weight: bold; margin-top: 18px;margin-bottom:9px;">Mode de paiement</p>
                              <div class="row">
                                    <div class="col-md-6">
                                        <img src="{{ asset('images/Capture_visazz.PNG') }}"  alt="visa carte" width="25px" height="22px">
                                        <input type="radio" name="paiement" id="visa" value="visa"  checked="checked" style="height: 19px;
                                        width: 26px;">
                                    </div>
                                    <div class="col-md-6" style="color: #000; font-weight: bold;" >

                                        Cash
                                        <input type="radio"  name="paiement" value="cash" id="cash" style="height: 19px;
                                        width: 26px;">
                                    </div>
                              </div>
                              </div>
                              <div style="    margin-top: 0px;
                              margin-bottom: 18px;
                              text-align: center;
                              background: #370274;
                              margin-left: 15px;
                              margin-right: 15px;
                              border-radius: 5px;">
                                <button type="submit" class="btn" style="color:white; font-weight: bold;    box-shadow: none;">Reserver</button>
                              </div>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('extrat-js')
    <script type="text/javascript">
            $(document).ready(function() {
				$("#dateArrive").datepicker({
					showOn: 'button',
					buttonText: 'Modifier',
					buttonImageOnly: false,
					dateFormat: 'd MM yy ',
					constrainInput: true
				});


                $("#dateDepart").datepicker({
					showOn: 'button',
					buttonText: 'Modifier',
					buttonImageOnly: false,
					dateFormat: 'd MM yy ',
					constrainInput: true
				});



            	$(".ui-datepicker-trigger").mouseover(function() {
					$(this).css('cursor', 'pointer');
				});
            });
    </script>
@stop
