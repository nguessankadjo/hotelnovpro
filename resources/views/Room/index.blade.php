@extends('shared._navBar')

{{-- @section('styles')
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@stop --}}

@section('content')

<div class="container">
    @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
    @endif
</div>

<section class="banner1">
    <div class="container ">
        <div class="row">
            <div>&nbsp;</div>
            <div class="col-md-10 offset-1 section1">
                <div class="row">
                    <div class="col-sm-6">
                        <h1 class="banner-h1" style="font-size: 69px; white-space:nowrap; font-weight: bolder;    text-shadow: 4px 7px 7px #000;" >Bienvenue chez vous !</h1>
                        <p  class="banner-p" style="font-size: 20px; font-weight: bolder;">Demarrer une reservation</p>
                    </div>
                    <div class="col-md-6">&nbump;</div>
                </div>
                <div class="row bg-white smd" style="border-radius: 6px; height: 70%; text-align: center;">
                    <div class="col-md-10 offset-1 ">
                        <div class="row block1" >
                            <div class="col-md-3" >
                                <h5 class="card-title header mb-3" style="    margin-left: -110px;">ARRIVEE</h5>
                                <div class="card mt-4 card-bande">
                                    <div class="card-body card-bande1">
                                        <small style="font-weight: bolder; font-size: 10px;" class="card-title">SAMEDI</small>
                                        <p class="card-text "><span style="font-size:45px; font-weight: bolder; color: #370274;"> 17</span><small style="color: black; font-weight: bolder;">AVR.2021</small></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h5 class="card-title header" style="    margin-left: -110px;" >DEPART</h5>
                                <div class="card mt-4 card-bande" >
                                    <div class="card-body card-bande1">
                                        <small style="font-weight: bolder; font-size: 10px;" class="card-title">DIMANCHE</small>
                                        <p class="card-text "><span style="font-size:45px; font-weight: bolder; color: #370274;"> 18</span><small style="color: black; font-weight: bolder;"> AVR.2021</small></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <h5 class="card-title header"  style="    margin-left: -110px;">INVITES</h5>
                                <div class="card mt-4 card-double" >
                                    <div class="card-body card-bande1">
                                        <small style="font-weight: bolder; font-size: 10px;" class="card-title "><span class="pr-4 pb-5">ADULTES</span> <span>ENFANTS</span></small>
                                        <div class="card-text mt-4 d-flex" style="text-align: center;">
                                            <div style="margin-left: 6px;
                                            margin-right: 34px;">
                                                <small style="background-color: #370274; border-radius: 4px; font-weight: bolder;padding: 9px 14px 10px 15px;
                                                text-align: center;
                                                color: white;
                                                font-size: 18px;" >2</small>
                                            </div>
                                            <div>
                                                <small  style="background-color: #370274; border-radius: 4px;font-weight: bolder;padding: 9px 14px 10px 15px;
                                                text-align: center;
                                                color: white;
                                                font-size: 18px;">1</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 voir-info">
                            <p class="card-text card-button"> <a href="#" class="btn" style="background-color: #370274; color: white; font-weight: bolder;margin-top: 140px;" >VOIR DISPONIBILITE</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container div-apopos" >
        <p class="" style="font-size: 42px; color:  #cacaca;font-family: genera, sans-serif;">A propos de </p>
        <div class="card " style="outline: none;border: none;  margin-top: -14px;">
            <div class="row apropos-text" >
                <div class="col-md-9" >
                    <h5 class="card-title mt-2 mb-5" style="font-size: 42px;font-weight: bolder; color:  #111010;font-family: system-ui;">NovHôtel Royal Suites</h5>
                    <p class="card-text mr-5" style="text-align: justify;">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Praesent eu venenatis nisl. Integer eu felis pulvinar, pellentesque augue sed,
                            faucibus nisi. Sed congue lacinia massa. Nullam ipsum lectus, dignissim et sem et,
                            viverra eleifend justo. Aliquam at sodales orci. Duis vestibulum, sapien vel maximus
                            hendrerit, eros nibh tempus sem, vel varius diam leo et lacus. Fusce libero metus,
                            finibus eu arcu vitae, condimentum malesuada dolor. Nam volutpat mattis lorem id ornare.
                            Proin dapibus massa at pellentesque pellentesque.  Sed congue lacinia massa. Nullam ipsum lectus, dignissim et sem et,
                            hendrerit, eros nibh tempus sem, vel varius diam leo et lacus. Fusce libero metus,
                            finibus eu arcu vitae, condimentum malesuada dolor. Nam volutpat mattis lorem id ornare.
                            Proin dapibus massa at pellentesque pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu venenatis nisl.
                            Integer eu felis pulvinar, pellentesque augue sed, faucibus nisi. Sed congue lacinia massa. Nullam ipsum lectus, dignissim et sem et,
                            Sed congue lacinia massa. Nullam ipsum lectus, dignissim et sem et,
                        </p>
                </div>
                <div class="col-md-3">
                        <img src="{{ asset('images/image3.png') }}" width="240px" height="100%">
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 96px;">
                <div class="col-md-9 non-suit">
                    <h1 style="font-family: genera, sans-serif; font-weight: bolder;" >Nos suites </h1>
                </div>
                <div class="col-md-3 d-flex">
                    <div class="mt-3" style="margin-right: 93px;">
                        <i class="fa fa-chevron-left" style="color: #004d8a;font-size: 19px;" aria-hidden="true"></i>
                    </div>
                    <div class="mt-3 ml-5" style="margin-right: 35px;">
                        <i class="fa fa-chevron-right" style="color: #004d8a;font-size: 19px;" aria-hidden="true"></i>
                    </div>
                    <div>
                        <h3 class="mt-3 ml-2" style="white-space:nowrap; font-size: 16px; font-weight: bolder;"> TOUT AFFICHER</h3>
                    </div>
                    <div class= "mt-3 ml-2">
                        <i class="fa fa-arrow-right" style="color: #67def2;margin-left: 10px;font-size: 19px;" aria-hidden="true"></i>
                    </div>
                </div>
        </div>
        <div class="card-group" style="margin-top:34px;" >
            <div class="row justify-content-between">
                   @foreach ( $roomAll as $room )
                   <a href="{{ route('showRoom', $room->id) }}" class="col-md-4" style="text-decoration: none;color: black">
                    <div class="card card-image" style="border-radius: 10px;">
                        <div>
                            <i class="far fa-heart icon-heart-room"></i>
                        </div>
                        <img src="{{ asset('images/imga6.png') }}" alt="image reservation 1" style="width:100%;border-radius: 10px;">
                        <div class="card-body" style="    margin-top: 10px;">
                            <h5 class="card-title card-title-group">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                                        <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                                        <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                </svg> <Small style="margin-top: -19px;
                                font-size: 12px;
                                color: gray;
                                opacity: 0.5;
                                margin-right: 26px;margin-left: 8px;">ASSINIE,Ivory Coast <em class="text-primary float-right" style="white-space:nowrap;margin-left: 140px;">Luxury villa</em></Small>
                            </h5>
                            <p class="card-text "><span style="font-weight: bolder; font-size: 26px;">{{ $room->prix }}</span> <sup style="font-weight: bolder;" class="mr-5">Frcfa</sup><small>/nuitée en semaine</small></p>
                            <p class="card-text "><small style="font-size: 22px;
                                font-weight: bold;
                                font-family: sans-serif;">{{ $room->titre }}</small></p>
                            <p class="card-text "> <i class="fa fa-user" style="margin-right: 8px;" aria-hidden="true"></i><small style="font-weight: bolder;">{{ $room->nombre_personne }} personnes</small></p>
                            <p class="card-text "><i class="fa fa-bed" style="margin-right: 8px;" aria-hidden="true"></i><small style="font-weight: bolder;">{{ $room->chambre }} chambres</small></p>
                            <p class="card-text "><i class="fa fa-bath" style="margin-right: 8px;" aria-hidden="true"></i><small style="font-weight: bolder;">{{ $room->bain }} salle de bain</small> </p>
                            <p class="card-text" style="margin-bottom: 16px;"><i class="fa fa-columns" style="margin-right: 8px;" aria-hidden="true"></i><small style="font-weight: bolder;">{{ $room->perimetre }} m2</small></p>
                        </div>
                    </div>
                   </a>
                   @endforeach
                </div>
        </div>
</div>

@include('shared.footer')

@endsection


