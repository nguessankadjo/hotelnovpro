@extends('layouts.app')

@section('content')
<div class="container">
    @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
    @endif
</div>
    <div class="container">

        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-success" href="{{ route('showAll') }}" style="    height: 34px;
                text-align: center;
                font-weight: bold;">Retour à la page d'accueil</a>
            </div >
            <div class="col-md-6">
                <h3 class="mb-5">Dashboard</h3>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Liste des réservations') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                  <th scope="col">id</th>
                                  <th scope="col">Date arrivé</th>
                                  <th scope="col">Date départ</th>
                                  <th scope="col">Nombre invité</th>
                                  <th scope="col">User</th>
                                  <th scope="col">chambre reservé</th>
                                  <th scope="col">Moyen de paiement</th>
                                  <th scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                  @foreach ($reservationAll as $reservation )
                                    <tr>
                                        <th scope="row">{{ $reservation->id }}</th>
                                        <td>{{ $reservation->date_arrive }}</td>
                                        <td>{{ $reservation->date_depart }}</td>
                                        <td>{{ $reservation->nombre_invite }}</td>
                                        <td>{{ $reservation->user->name }}</td>
                                        <td>{{ $reservation->room->titre }}</td>
                                        <td>{{ $reservation->paiement }}</td>
                                        <td>
                                            <form action="{{ route('home.supprimer', $reservation->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"><i style="color: red" class="fa fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                  @endforeach
                              </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
