<?php

use App\Http\Controllers\ReservationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', [App\Http\Controllers\HomeController::class, 'showAll'])->name('showAll');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::delete('/home/{id}', [App\Http\Controllers\HomeController::class, 'destroy'])->name('home.supprimer');



Route::get('/showRoom/{id}', [App\Http\Controllers\HomeController::class, 'showRoomByID'])->name('showRoom');


Route::get('/showReservation/{id}', [App\Http\Controllers\HomeController::class, 'showReservation'])->name('showReservation');

Route::post('/showReservation/{id}', [App\Http\Controllers\HomeController::class, 'saveReservation'])->name('saveReservation.create');





