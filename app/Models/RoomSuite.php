<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomSuite extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function reservationRooms()
    {
        return $this->hasMany(Reservation::class);
    }
}
