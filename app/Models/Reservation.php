<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
     'paiement',
     'date_arrive',
     'date_depart',
     'nombre_invite' ,
      'user_id' ,
       'room_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function room()
    {
        return $this->belongsTo(RoomSuite::class,'room_id');
    }
}
