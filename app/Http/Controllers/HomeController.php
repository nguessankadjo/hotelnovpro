<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\RoomSuite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index( )
    {
        $reservationAll = Reservation::all();

        return view('home',compact('reservationAll'));
    }

    public function destroy($id)
    {
        Reservation::find($id)->delete();

        return redirect()->route('home')
        ->with('success','La reservation à été supprimer avec succés.  Merci !!');
    }


    public function showAll()
    {

        $roomAll = RoomSuite::all();

        return view('Room.index',compact('roomAll'));
    }


    public function showRoomByID($id)
    {
        $getRoom = RoomSuite::where("id", "=", $id)->get();

        $getRoomid =$getRoom['0'];

        // dd($getRoomid);

        return view('Room.detailRoom',compact('getRoomid'));
    }



    public function showReservation( $id)
    {
        $getRoom = RoomSuite::where("id", "=", $id)->get();

        $getRoomid =$getRoom['0'];

        // dd($id);
        return view('Room.reservation',compact('id','getRoomid'));
    }


    public function saveReservation(Request $request, $id)
    {
        $getRoom = RoomSuite::where("id", "=", $id)->get();

        $getRoomid =$getRoom['0'];

                $this->validate($request,[
                    'date_arrive' => 'required',
                    'date_depart' => 'required',
                    'paiement' => 'required',
                    'nombre_invite' => 'required',
                    ]);

                    $input = $request->all();
                    $input['user_id'] = Auth::user()->id;
                    $input['room_id'] = $id;

                    Reservation::create($input);

        return redirect()->route('showAll')->with('success','La reservation a bien été éffectuée. Merci !!');
    }


}
