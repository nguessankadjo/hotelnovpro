<?php

namespace Database\Seeders;

use App\Models\RoomSuite;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class RoomSuiteTableSeeder extends Seeder
{

    public function run()
    {
        $faker = \Faker\Factory::create();

        $limit = 3;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('room_suites')->insert([
                'titre' => $faker->streetName,
                'description' => $faker->paragraphs(5,true),
                'prix' => $faker->numberBetween(25000,350000),
                'nombre_personne' => $faker->numberBetween(8,17),
                'chambre' =>$faker->numberBetween(4,6),
                'bain' => $faker->numberBetween(2,4),
                'perimetre' => $faker->numberBetween(42,80),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
