<?php

namespace Database\Factories;

use App\Models\RoomSuite;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoomSuiteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = RoomSuite::class;


    public function definition()
    {
        return [
            // 'titre' => $this->faker->streetName,
            // 'description' =>  $this->faker->paragraphs(5),
            // 'prix' =>  $this->faker->numberBetween(25000,350000),
            // 'nombre_personne' =>  $this->faker->numberBetween(8,17),
            // 'chambre' => $this->faker->numberBetween(4,6),
            // 'bain' =>  $this->faker->numberBetween(2,4),
            // 'perimetre' => $this->faker->numberBetween(42,80),
            // 'created_at' => \Carbon\Carbon::now(),
            // 'Updated_at' => \Carbon\Carbon::now(),
        ];
    }
}
