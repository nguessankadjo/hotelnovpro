<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomSuitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_suites', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->float('prix');
            $table->integer('nombre_personne');
            $table->integer('bain');
            $table->integer('chambre');
            $table->text('description');
            $table->decimal('perimetre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_suites');
    }
}
